<?php

namespace Drupal\php_profiler\XHProfLib\Storage;

use Drupal\xhprof\XHProfLib\Storage\StorageInterface;

/**
 * Provides XHGui Upload storage backend for xhprof runs.
 */
class XhGuiUploader implements StorageInterface {

  /**
   * XHGuid base URL.
   *
   * @var string
   */
  private string $url;

  /**
   * XHGui token.
   *
   * @var string
   */
  private string $token;

  /**
   * HTTP Client.
   *
   * @var object
   */
  private object $httpClient;

  /**
   * Construct the storage backend.
   *
   * @param string|null $url
   *   URL to the XHGui web application.
   * @param string|null $token
   *   The token for the XHGui import.
   */
  public function __construct(string $url = NULL, string $token = NULL) {
    // phpcs:ignore
    $factory = \Drupal::configFactory();
    $configUrl = $factory->get('xhprof.config')->get('xhgui_url');
    $configToken = $factory->get('xhprof.config')->get('xhgui_token');
    $this->url = $url ?? $configUrl ?? '';
    $this->token = $token ?? $configToken ?? '';
    $this->url .= !empty($this->url) ? '/run/import' : '';
    // phpcs:ignore
    $this->httpClient = \Drupal::service('http_client');
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'XHGui Upload';
  }

  /**
   * {@inheritdoc}
   */
  public function getRun($run_id, $namespace) {
    // Runs are external so they can't be loaded.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRuns($namespace = NULL) {
    // Runs are external so they can't be loaded.
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function saveRun($data, $namespace, $run_id) {
    // Prepare run data.
    $json_data = json_encode($this->prepareProfilingData($data));

    // Prepare HTTP request.
    $headers = [
      'Accept' => 'application/json',
      'Content-Type' => 'application/json',
    ];
    $url = $this->getUploadUrl();
    try {
      $response = $this->httpClient->post(
        $url, [
          'headers' => $headers,
          'body' => $json_data,
        ]);
      $response_data = json_decode($response->getBody()->getContents(), TRUE);

      // Do something with data.
      // @todo
    }
    catch (Exception $e) {
      // Log exception.
      // @todo log to watchdog.
      $e = $e;
    }

    return $run_id;
  }

  /**
   * Get the URL to upload the data to.
   *
   * Optionally includes the auth token if set.
   */
  protected function getUploadUrl() {
    if (!empty($token)) {
      return $this->url . '?&token=' . $this->token;
    }
    return $this->url;
  }

  /**
   * @return array
   */
  public function prepareProfilingData(array $profile) {
    $url = $this->getUrl();
    $requestTimeFloat = explode('.', $_SERVER['REQUEST_TIME_FLOAT']);
    if (!isset($requestTimeFloat[1])) {
      $requestTimeFloat[1] = 0;
    }

    $allowedServerKeys = [
      'DOCUMENT_ROOT',
      'HTTPS',
      'HTTP_HOST',
      'HTTP_USER_AGENT',
      'PATH_INFO',
      'PHP_AUTH_USER',
      'PHP_SELF',
      'QUERY_STRING',
      'REMOTE_ADDR',
      'REMOTE_USER',
      'REQUEST_METHOD',
      'REQUEST_TIME',
      'REQUEST_TIME_FLOAT',
      'SERVER_ADDR',
      'SERVER_NAME',
      'UNIQUE_ID',
    ];

    $serverMeta = array_intersect_key($_SERVER, array_flip($allowedServerKeys));

    $meta = [
      'url' => $url,
      'get' => $_GET,
      'env' => $this->getEnvironment($_ENV),
      'SERVER' => $serverMeta,
      'simple_url' => $this->getSimpleUrl($url),
      'request_ts_micro' => ['sec' => $requestTimeFloat[0], 'usec' => $requestTimeFloat[1]],
        // These are superfluous and should be dropped in the future.
      'request_ts' => ['sec' => $requestTimeFloat[0], 'usec' => 0],
      'request_date' => date('Y-m-d', $requestTimeFloat[0]),
    ];

    $data = [
      'profile' => $profile,
      'meta' => $meta,
    ];

    return $data;
  }

  /**
   * @param array $env
   * @return array
   */
  private function getEnvironment(array $env) {
    foreach ($this->excludeEnv as $key) {
      unset($env[$key]);
    }

    return $env;
  }

  /**
   * Creates a simplified URL given a standard URL.
   * Does the following transformations:
   *
   * - Remove numeric values after =.
   *
   * @param string $url
   *
   * @return string
   */
  private function getSimpleUrl($url) {
    if (is_callable($this->simpleUrl)) {
      return call_user_func($this->simpleUrl, $url);
    }

    return preg_replace('/=\d+/', '', $url);
  }

  /**
   * @return string
   */
  private function getUrl() {
    $url = array_key_exists('REQUEST_URI', $_SERVER) ? $_SERVER['REQUEST_URI'] : NULL;
    if (!$url && isset($_SERVER['argv'])) {
      $cmd = basename($_SERVER['argv'][0]);
      $url = $cmd . ' ' . implode(' ', array_slice($_SERVER['argv'], 1));
    }

    if (is_callable($this->replaceUrl)) {
      $url = call_user_func($this->replaceUrl, $url);
    }

    return $url;
  }

}
